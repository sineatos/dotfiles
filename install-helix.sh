#! /bin/bash

# lsp: https://github.com/helix-editor/helix/wiki/Language-Server-Configurations

helix_conf_dir=$(pwd)/helix
home_dir=$HOME
target_helix_conf_dir=${home_dir}/.config/helix

current_time="$(date +%Y%m%d%H%M%S)"

if [ -e "${helix_confdir}" ]; then
    bak_path=""    
fi
conf_list=(".tmux.conf" ".tmux.conf.local")
for conf in "${conf_list[@]}"; do
    if [ -e "${target_helix_conf_dir}" ]; then
        bak_path="$target_helix_conf_dir-bak-$current_time"
        echo "$target_helix_conf_dir exist, mv $target_helix_conf_dir -> $bak_path"
        mv "$target_helix_conf_dir" "$bak_path"
    fi
    cp -r "$helix_conf_dir" "$target_helix_conf_dir"
done
