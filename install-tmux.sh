#! /bin/bash

tmux_conf_dir=$(pwd)/tmux
home_dir=$HOME
current_time="$(date +%Y%m%d%H%M%S)"

# 基础配置文件
conf_list=(".tmux.conf" ".tmux.conf.local")
for conf in "${conf_list[@]}"; do
    if [ -e "$home_dir/$conf" ]; then
        bak_path="$home_dir/$conf-bak-$current_time"
        echo "$home_dir/$conf exist, mv $home_dir/$conf -> $bak_path"
        mv "$home_dir/$conf" "$bak_path"
    fi
    cp "$tmux_conf_dir/$conf" "$home_dir/$conf"
done

# # 个人扩展配置
# tmux_conf_list=("menus")
# for conf in "${tmux_conf_list[@]}"; do
#     if [ -e "$home_dir/.tmux/$conf" ]; then
#         bak_path="$home_dir/.tmux/$conf-bak-$current_time"
#         echo "$home_dir/.tmux/$conf exist, mv $home_dir/.tmux/$conf -> $bak_path"
#         mv "$home_dir/.tmux/$conf" "$bak_path"
#     fi
#     cp -r "$tmux_conf_dir/.tmux/$conf" "$home_dir/.tmux/$conf"
# done
