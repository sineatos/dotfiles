#! /bin/bash

nerd_conf_dir=$(pwd)/attachments/nerdfonts
home_dir=$HOME
target_dir=/usr/share/fonts/nerdfonts
sudo mkdir -p "$target_dir"

sudo cp -r "${nerd_conf_dir}/*" "${target_dir}/"
sudo chmod 644 "${target_dir}/*"

# sudo apt install -y xfonts-utils fontconfig
sudo mkfontscale
sudo mkfontdir
sudo fc-cache -fv
