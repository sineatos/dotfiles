# dotfiles

my dot files such as tmux, vim etc 

## 脚本说明

- `install-tmux.sh`: 自动备份原来的配置, 然后将tmux配置复制到HOME目录中

## tmux

`tmux/`中保存tmux配置, 该配置参考 [https://github.com/gpakosz/.tmux ](https://github.com/gpakosz/.tmux) 并按照自身使用习惯和使用环境进行了部分调整和修改

尽量不要修改`.tmux.conf`, 个人配置尽量放在`.tmux.conf.local`中

配置中已启用tpm管理配置, 但配置方式与原版的tpm优点不一样, 建议查看`.tmux.conf.local`中说明
