#! /bin/bash

# 获取当前脚本的绝对路径
SCRIPT_PATH=$(realpath "$0")

# 获取当前脚本所在的目录
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

ln -s ${SCRIPT_DIR}/nvim ~/.config/nvim
