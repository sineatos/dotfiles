-- 自定义配置

local M = {
  -- 是否开启调试模式
  debug_mode = false,
  -- github代理前缀
  -- github_proxy_prefix = "https://ghproxy.cn/",
}

return M

