-- 代理相关方法

local inori_config = require('inori.config').inori_config

local M = {}

-- 将url批量转为代理url
function M.batch_get_proxy_url(urls)
  if urls == nil then
    return nil
  end
  local proxy_urls = {}
  for index, value in ipairs(urls) do
    proxy_urls[index] = inori_config.github_proxy_prefix .. value
  end

  return proxy_urls
end

-- 将url转为代理url
function M.get_proxy_url(url)
  if url == nil then
    return nil
  end
  return M.batch_get_proxy_url({ url })[1]
end

-- 获取github代理后的格式
function M.get_github_proxy_url_format()
  local prefix = inori_config.github_proxy_prefix or ""
  if prefix == "" then
    return "https://github.com/%s.git"
  end
  return prefix .. "https://github.com/%s"
end

-- 获取github代理前缀
function M.get_github_proxy_prefix()
  return inori_config.github_proxy_prefix or ""
end

return M

