-- inori 通用工具函数

local json = require("vendor.json")

local M = {}

-- 如果文件存在, 则读取文件中的文本内容, 否则返回 nil
function M.read_text_file_if_exist(file_path)
    local file = io.open(file_path, "r")
    if not file then
        return nil
    end

    local content = file:read("*all")
    file:close()

    return content
end

-- 如果lua脚本存在, 则以table的形式导入并返回, 否则返回空表
function M.load_table_if_exist(file_path)
    if pcall(require, file_path) then
        return require(file_path)
    else
        return {}
    end
end

-- 拼接路径
function M.path_join(file_paths)
    local result = ''
    for index, value in ipairs(file_paths) do
        if index == 1 then
            result = value
        else
            result = result .. '/' .. value
        end
    end
    return result
end



-- 输出信息
-- @param messages: 一个消息列表, 最终会将它们拼成一个字符串输出
-- @param wait_press_key: 是否要等待用户输入, 默认 false
function M.print_msg(messages, wait_press_key)
    local message_str = ""
    if type(messages) ~= "table" then
        messages = { messages }
    end
    for _, message in ipairs(messages) do
        if type(message) == "table" then
            message_str = message_str .. " " .. json.encode(message)
        else
            message_str = message_str .. " " .. tostring(message)
        end
    end
    vim.api.nvim_echo({{message_str}}, true, {})
    if wait_press_key then
        vim.api.nvim_echo({
            { "\nPress any key to exit..." },
          }, true, {})
        vim.fn.getchar()
    end
end

return M