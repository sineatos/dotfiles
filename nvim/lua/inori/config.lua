-- inori 默认配置

SYSTEM_WINDOWS = 'windows'
SYSTEM_LINUX = 'linux'
SEP = '/'

local M = {}
local utils = require("inori.utils")

-- 默认配置
local default_config = {
    -- 是否开启调试模式, 开启调试模式后会输出相关信息
    debug_mode = false,
    -- github代理前缀, 默认 '' 表示 不走代理
    -- 代理的设置可以参考: https://github.akams.cn/
    github_proxy_prefix = '',
}

-- 全局变量
M.G = {}
if package.config.sub(1,1) == '\\' then
    M.G.system = SYSTEM_WINDOWS
    M.G.home = os.getenv('USERPROFILE')
else
    M.G.system = SYSTEM_LINUX
    M.G.home = os.getenv('HOME')
end
M.G.config_path = utils.path_join({M.G.home, '.config'})
M.G.nvim_config_path = utils.path_join({M.G.config_path, 'nvim'})

-- 加载自定义配置
function M.load_custom_configs()
    local inori_config_path = 'inori.custom'
    if pcall(require, inori_config_path) then
        return require(inori_config_path)
    end
    return {}
end

-- 刷新配置
function M.refresh_configs()
    local final_configs = {}
    local cc = M.load_custom_configs() or {}
    for key, value in pairs(default_config) do
        final_configs[key] = value
    end
    for key, value in pairs(cc) do
        final_configs[key] = value
    end
    M.inori_config = final_configs
end

M.refresh_configs()

-- 输出配置信息
if M.inori_config.debug_mode then
    utils.print_msg("[debug] enable debug mode...")
    utils.print_msg({"[debug] inori config:", M.inori_config,})
end

return M