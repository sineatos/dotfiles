-- blink-cmp 配置

--[[
local inori_config = require("inori.config").inori_config
local proxy = require("inori.proxy")
local utils = require("inori.utils")
local blink_download = require('blink.cmp.fuzzy.download')
local download_config = require('blink.cmp.config').fuzzy.prebuilt_binaries


return {
  "saghen/blink.cmp",
  init = function(plugin)
    -- 重写从github拉取的逻辑
    blink_download.from_github = function(tag, cb)
      blink_download.get_system_triple(function(system_triple)
        if not system_triple then
          return cb(
            'Your system is not supported by pre-built binaries. You must run cargo build --release via your package manager with rust nightly. See the README for more info.'
          )
        end

        local url = proxy.get_github_proxy_prefix() .. 'https://github.com/saghen/blink.cmp/releases/download/'
            .. tag
            .. '/'
            .. system_triple
            .. blink_download.get_lib_extension()

        -- if inori_config.debug_mode then
        utils.print_msg({ "[debug]", "blink-cmp: ", url })
        -- end

        local args = { 'curl' }
        vim.list_extend(args, download_config.extra_curl_args)
        vim.list_extend(args, {
          '--fail',       -- Fail on 4xx/5xx
          '--location',   -- Follow redirects
          '--silent',     -- Don't show progress
          '--show-error', -- Show errors, even though we're using --silent
          '--create-dirs',
          '--output',
          blink_download.lib_path,
          url,
        })

        vim.system(args, {}, function(out)
          if out.code ~= 0 then return cb('Failed to download pre-build binaries: ' .. out.stderr) end
          cb()
        end)
      end)
    end
  end,
}
-- ]]

return {}
