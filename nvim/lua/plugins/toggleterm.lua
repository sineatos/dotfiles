-- 终端配置

local proxy = require("inori.proxy")

-- 源项目地址: https://github.com/akinsho/toggleterm.nvim
-- 可参考镜像项目: https://gitcode.com/gh_mirrors/to/toggleterm.nvim/overview
local opts = {
  size = 20,
  open_mapping = [[<c-\>]],
  hide_numbers = true,
  shade_filetypes = {},
  shade_terminals = true,
  shading_factor = 1,
  start_in_insert = true,
  insert_mapppings = true,
  persist_size = true,
  direction = "float",
  close_on_exit = true,
  shell = vim.o.shell,
  float_opts = {
    border = "single",
    winblend = 3,
    highlights = {
      border = "Normal",
      background = "Normal",
    },
  },
}

return {
  "akinsho/toggleterm.nvim",
  opts = opts,
}
