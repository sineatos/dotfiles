-- 插件代理设置:
-- 因为一个插件有可能对应到不同的功能中, 且lazyvim会对一部分配置进行覆盖
-- 该配置文件专门对url进行配置代理地址
-- 由于插件是通过 lazy.nvim 管理的, 该插件提供了设置 git.url_format 的设置, 一般情况下, 该选项已经足够使用了, 此处的配置更多的是为了处理其它仓库需要走代理的情况

local inori_config = require("inori.config").inori_config
local utils = require("inori.utils")
local proxy = require("inori.proxy")

-- 所有代理配置
local configs = {}

-- 根据配置选择对应的方式完成代理
local function execute_proxy_process(plugin_cfgs)
    if inori_config.github_proxy_prefix == "" then
        return {}
    end

    if inori_config.debug_mode then
        utils.print_msg({"[debug]", "plugin use proxy"})
    end

    for _, plugin_cfg in ipairs(plugin_cfgs) do
        plugin_cfg["url"] = proxy.get_proxy_url("https://github.com/" .. plugin_cfg[1])
    end
    return plugin_cfgs
end

return execute_proxy_process(configs)
