-- tree-sitter 配置
-- 相关配置可参考：https://gitee.com/zgpio/nvim-treesitter

local inori_config = require("inori.config").inori_config
local proxy = require("inori.proxy")
local utils = require("inori.utils")
local parsers = require("nvim-treesitter.parsers")
local github_proxy_prefix = proxy.get_github_proxy_prefix() .. "https://github.com/"

-- 设置拉去解析器的url镜像
local set_parsers_mirror = function()
  for _, config in pairs(parsers.get_parser_configs()) do
    local replace_url = config.install_info.url:gsub("https://github.com/", github_proxy_prefix)
    config.install_info.url = replace_url
    if inori_config.debug_mode then
      utils.print_msg({ "[debug]", "tree-sitter url: ", config.install_info.url })
    end
  end
end

local init_function = function(plugin)
  -- PERF: add nvim-treesitter queries to the rtp and it's custom query predicates early
  -- This is needed because a bunch of plugins no longer `require("nvim-treesitter")`, which
  -- no longer trigger the **nvim-treesitter** module to be loaded in time.
  -- Luckily, the only things that those plugins need are the custom queries, which we make available
  -- during startup.
  require("lazy.core.loader").add_to_rtp(plugin)
  require("nvim-treesitter.query_predicates")

  -- 以下才是自定义的逻辑
  set_parsers_mirror()
  -- 改用git拉去, 不使用curl拉取
  -- require("nvim-treesitter.install").prefer_git = true
end

return {
  "nvim-treesitter/nvim-treesitter",
  init = init_function,
  opts = {
    -- Automatically install missing parsers when entering buffer
    -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
    auto_install = false,

  },
}
