-- 原项目: https://github.com/kawre/leetcode.nvim
-- 参考: https://gitcode.com/gh_mirrors/le/leetcode.nvim/overview
-- 启动方式: nvim leetcode.nvim
-- 登录方式: 先登录leetcode，打开f12, 刷新页面，找到 graphql/ 的请求，在里面找到Cookie选项
return {
  "kawre/leetcode.nvim",
  build = ":TSUpdate html",
  dependencies = {
    "nvim-telescope/telescope.nvim",
    "nvim-lua/plenary.nvim", -- telescope 所需
    "MunifTanjim/nui.nvim",

    -- 可选
    "nvim-treesitter/nvim-treesitter",
    "rcarriga/nvim-notify",
    "nvim-tree/nvim-web-devicons",
  },
  opts = {
    -- 配置放在这里
    cn = {
      enabled = true,
    },
  },
}
