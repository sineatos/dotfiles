-- meson 配置自定义

local proxy = require("inori.proxy")

-- meson 相关配置请参考: https://github.com/williamboman/mason.nvim
-- gitee 镜像配置参考: https://gitee.com/neotaich/mason.nvim
local opts = {
  -- Limit for the maximum amount of packages to be installed at the same time. Once this limit is reached, any further
  -- packages that are requested to be installed will be put in a queue.
  max_concurrent_installers = 8,
  github = {
    ---@since 1.0.0
    -- The template URL to use when downloading assets from GitHub.
    -- The placeholders are the following (in order):
    -- 1. The repository (e.g. "rust-lang/rust-analyzer")
    -- 2. The release version (e.g. "v0.3.0")
    -- 3. The asset name (e.g. "rust-analyzer-v0.3.0-x86_64-unknown-linux-gnu.tar.gz")
    download_url_template = proxy.get_github_proxy_prefix() .. "https://github.com/%s/releases/download/%s/%s",
  },
  ui = {
    -- Whether to automatically check for new versions when opening the :Mason window.
    check_outdated_packages_on_open = false,
  },
}

return {
  "williamboman/mason.nvim",
  opts = opts,
}

