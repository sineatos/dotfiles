
-- load inori config
require("inori.config")

-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")